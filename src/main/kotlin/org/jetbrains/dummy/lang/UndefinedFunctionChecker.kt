package org.jetbrains.dummy.lang

import org.jetbrains.dummy.lang.tree.DummyLangVisitor
import org.jetbrains.dummy.lang.tree.Element
import org.jetbrains.dummy.lang.tree.File
import org.jetbrains.dummy.lang.tree.FunctionCall

class UndefinedFunctionChecker(private val reporter: DiagnosticReporter) : AbstractChecker() {
    override fun inspect(file: File) {
        file.accept(UndefinedFunctionVisitor(), file.functions.map { it.name }.toSet())
    }

    private fun reportUndefinedFunction(functionCall: FunctionCall) {
        reporter.report(functionCall, "Function '${functionCall.function}' is not defined")
    }

    private inner class UndefinedFunctionVisitor : DummyLangVisitor<Unit, Set<String>>() {
        override fun visitElement(element: Element, data: Set<String>) {
            element.acceptChildren(this, data)
        }

        override fun visitFunctionCall(functionCall: FunctionCall, data: Set<String>) {
            functionCall.acceptChildren(this, data)
            if (functionCall.function !in data) {
                reportUndefinedFunction(functionCall)
            }
        }
    }
}