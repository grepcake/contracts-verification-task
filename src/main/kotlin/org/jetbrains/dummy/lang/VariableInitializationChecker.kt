package org.jetbrains.dummy.lang

import org.jetbrains.dummy.lang.VariableInitializationChecker.VisitResult.Assigned
import org.jetbrains.dummy.lang.VariableInitializationChecker.VisitResult.Returned
import org.jetbrains.dummy.lang.tree.*

class VariableInitializationChecker(private val reporter: DiagnosticReporter) : AbstractChecker() {
    override fun inspect(file: File) {
        file.accept(VariableInitializationVisitor(), LayeredSet())
    }

    // Use this method for reporting errors
    private fun reportAccessBeforeInitialization(access: VariableAccess) {
        reporter.report(access, "Variable '${access.name}' is accessed before initialization")
    }

    private inner class VariableInitializationVisitor : DummyLangVisitor<VisitResult, LayeredSet<String>>() {
        override fun visitElement(element: Element, data: LayeredSet<String>): VisitResult {
            element.acceptChildren(this, data)
            return EMPTY_RESULT
        }

        override fun visitFunctionDeclaration(
            functionDeclaration: FunctionDeclaration,
            data: LayeredSet<String>
        ): VisitResult {
            return functionDeclaration.body.accept(this, LayeredSet(functionDeclaration.parameters.toSet(), data))
        }

        override fun visitBlock(block: Block, data: LayeredSet<String>): VisitResult {
            val assigned = mutableSetOf<String>()
            val scopeAssigned = mutableSetOf<String>()
            val scopeUnassigned = mutableSetOf<String>()
            val makeData = { LayeredSet(scopeAssigned, LayeredSet(assigned, data)) }

            for (stmt in block.statements) {
                if (stmt is VariableDeclaration) { // maybe return 'ScopeChange' which includes unassigned values as well
                    scopeUnassigned.add(stmt.name)
                }
                val stmtResult = stmt.accept(this, makeData())
                when (stmtResult) {
                    is Returned -> return Returned
                    is Assigned -> for (assignment in stmtResult.values) {
                        if (scopeUnassigned.remove(assignment)) {
                            scopeAssigned.add(assignment)
                        } else {
                            assigned.add(assignment)
                        }
                    }
                }
            }

            return Assigned(assigned)
        }

        override fun visitAssignment(assignment: Assignment, data: LayeredSet<String>): VisitResult {
            assignment.acceptChildren(this, data)
            return Assigned(setOf(assignment.variable))
        }

        override fun visitIfStatement(ifStatement: IfStatement, data: LayeredSet<String>): VisitResult {
            ifStatement.condition.accept(this, data)
            val thenResult = ifStatement.thenBlock.accept(this, data)
            val elseResult = ifStatement.elseBlock?.accept(this, data) ?: EMPTY_RESULT
            return when (thenResult) {
                is Returned -> when (elseResult) {
                    is Returned -> Returned
                    is Assigned -> elseResult
                }
                is Assigned -> when (elseResult) {
                    is Returned -> thenResult
                    is Assigned -> Assigned(elseResult.values intersect thenResult.values)
                }
            }
        }

        override fun visitVariableDeclaration(
            variableDeclaration: VariableDeclaration,
            data: LayeredSet<String>
        ): VisitResult {
            variableDeclaration.acceptChildren(this, data)
            return if (variableDeclaration.initializer != null) {
                Assigned(setOf(variableDeclaration.name))
            } else {
                EMPTY_RESULT
            }
        }

        override fun visitReturnStatement(returnStatement: ReturnStatement, data: LayeredSet<String>): VisitResult {
            returnStatement.acceptChildren(this, data)
            return Returned
        }

        override fun visitVariableAccess(variableAccess: VariableAccess, data: LayeredSet<String>): VisitResult {
            if (variableAccess.name !in data) {
                reportAccessBeforeInitialization(variableAccess)
            }
            return EMPTY_RESULT
        }
    }

    private sealed class VisitResult {
        object Returned : VisitResult()
        class Assigned(val values: Set<String> = emptySet()) : VisitResult()
    }

    // Since there are rarely many nested scopes in a program,
    // checking a hierarchy of sets is a small cost for constant-time copying.
    private class LayeredSet<E>(val set: Set<E> = emptySet(), private val parent: LayeredSet<E>? = null) {
        operator fun contains(element: E): Boolean = element in set || parent?.contains(element) ?: false
    }

    companion object {
        private val EMPTY_RESULT = Assigned()
    }
}

